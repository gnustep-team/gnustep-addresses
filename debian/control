Source: addresses-for-gnustep
Priority: optional
Maintainer: Debian GNUstep maintainers <pkg-gnustep-maintainers@lists.alioth.debian.org>
Uploaders:
 Eric Heintzmann <heintzmann.eric@free.fr>,
 Yavor Doganov <yavor@gnu.org>,
Build-Depends:
 debhelper-compat (= 13),
 dpkg-build-api (= 1),
 imagemagick,
 libinspector-dev (>= 1.0.0-5),
Standards-Version: 4.7.0
Section: mail
Homepage: https://www.nongnu.org/gap/addresses
Vcs-Git: https://salsa.debian.org/gnustep-team/gnustep-addresses.git
Vcs-Browser: https://salsa.debian.org/gnustep-team/gnustep-addresses

Package: addressmanager.app
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 gnumail.app,
Description: Personal Address Manager for GNUstep
 This package constitutes a personal address manager for the GNUstep
 software system. It allows archiving complete personal contact
 information, organizing contacts in groups, integration with other
 software such as mail clients and sharing address information with
 other users over the network.

Package: libaddresses-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libaddresses0 (= ${binary:Version}),
 libgnustep-base-dev,
 ${misc:Depends},
Description: Database API backend framework for GNUstep (development files)
 This backend provides complete access to address information for
 applications. It is source-code compatible with Apple
 Corporation's AddressBook.framework.
 .
 This package contains the development files.

Package: libaddresses0
Section: libs
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Database API backend framework for GNUstep (library files)
 This backend provides complete access to address information for
 applications. It is source-code compatible with Apple
 Corporation's AddressBook.framework.
 .
 This package contains the runtime libraries.

Package: libaddressview-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libaddresses-dev,
 libaddressview0 (= ${binary:Version}),
 libgnustep-gui-dev,
 ${misc:Depends},
Description: Address display/edit framework for GNUstep (development files)
 This framework provides specialized view classes to applications
 which want to display addresses to the user in a graphical form.
 .
 This package contains the development files.

Package: libaddressview0
Section: libs
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Address display/edit framework for GNUstep (library files)
 This framework provides specialized view classes to applications
 which want to display addresses to the user in a graphical form.
 .
 This package contains the runtime libraries.

Package: addresses-goodies-for-gnustep
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Enhances:
 gworkspace.app
Description: Personal Address Manager for GNUstep (Goodies)
 This package contains a couple of things that might be of use:
  adgnumailconverter
   A tool that will merge your GNUMail address book into the Addresses
   database.
 .
  adserver
   A stand-alone Addresses network server.
 .
  adtool
   A command-line tool for address database manipulation.
 .
  VCFViewer
   A GWorkspace inspector for viewing .vcf files.
